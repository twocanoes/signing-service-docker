#!/bin/bash
set -e

printf "create new certificate and secrets? This will cause issues for existing databases. [y/N]:"
read create_new_cert

if [  "${create_new_cert}" == "y" ]; then
	printf "server name [sign.local]:"
	read  server_name
	
	if [ "${server_name}" == "" ]; then
		server_name="sign.local"
	fi
	echo "generating mysql root password"
	mysql_root_password=`openssl rand -hex 16`
	echo "generating mysql user password"
	mysql_user_password=`openssl rand -hex 16`
	echo "generating secret key base"
	secret_key_base=`openssl rand -hex 64`
	
	if [ -e signing_server_key.pem ]; then 
		
		mv signing_server_key.pem signing_server_key.pem_aside_$(date +"%Y%m%d_%H%M%S")
	fi
	if [ -e signing_server_cert.pem ]; then 
		
		mv signing_server_cert.pem signing_server_cert.pem_aside_$(date +"%Y%m%d_%H%M%S")
	fi
	
	
	openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 \
	-subj "/CN=${server_name}/" -days 365 -nodes \
	-keyout signing_server_key.pem  -out signing_server_cert.pem
	
	hex_cert=`cat signing_server_cert.pem |xxd -p -c 0|tr -d '\n'`
	
	hex_key=`cat signing_server_key.pem |xxd -p -c 0|tr -d '\n'`
	if [ -e docker-compose.yml ]; then 
		
		mv docker-compose.yml docker-compose.yml_aside_$(date +"%Y%m%d_%H%M%S")
	fi
	
	cat docker-compose-template.yml | sed "s/{{SSLServerCertificate}}/$hex_cert/" | sed "s/{{SSLServerKey}}/$hex_key/" | sed "s/{{MYSQL_ROOT_PASSWORD}}/$mysql_root_password/"  | sed "s/{{MYSQL_PASSWORD}}/$mysql_user_password/"  | sed "s/{{SECRET_KEY_BASE}}/$secret_key_base/" > docker-compose.yml
	

fi
