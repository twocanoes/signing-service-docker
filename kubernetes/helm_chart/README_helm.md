Signing Service Setup

1. Open Sharing and set computer name in Sharing to "signingservice". Turn on and off Screen Sharing to enable Bonjour.

	**Successful result: can ping signingservice.local**
	
1. Install Docker Desktop from [https://www.docker.com/products/docker-desktop/](https://www.docker.com/products/docker-desktop/) and launch Docker from the Applications folder.

1. Open Docker preferences, Under Kubernetes, Enable Kubernetes and click Apply & Restart. Click Install on the presented dialog.

1. Open Safari and go to [https://bitbucket.org/twocanoes/signing-service-docker/downloads/](https://bitbucket.org/twocanoes/signing-service-docker/downloads/) and Download Repository and Unzip. Rename downloaded folder to "signing-service" and move to the desktop.

1. Open Terminal and cd to `kubernetes/helm_chart/signing-service` in the `signing-service` folder.

1. Install the ssl certificate by running:

	`kubectl create secret tls tls-secret --cert=sample-tls/certificate.crt --key=sample-tls/certificate.key`

	**successful result: secret/tls-secret created**

1. log into docker by running this command in Terminal:

	`docker login`

	provide these credentials:

	username: `twocanoessoftware`
	
	readonly repo password: `e21d3b32-b731-47b1-a16d-b99c4e685486`

	**successful result: Login Succeeded.**

1. Pull the docker image by running:

	`docker pull twocanoessoftware/signing-service:app_v1`

	**successful result: No errors**

1. Install helm:

	`curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3`
	
	`chmod 700 get_helm.sh`
	
	`./get_helm.sh`
	
	**successful result: No errors**

1. Install Signing Service web app by running:

	`helm install sign .`

	**successful result: No errors**

1. Wait until apps are running by checking:

	`kubectl get pods`

	successful result: ready shows 1/1 for two services 

1. Setup database:

	`kubectl exec $(kubectl get pods |grep signing-service|awk '{print $1}') -- bash -c  '/var/www/app/bin/rake db:migrate'`

	Successful result: lots of lines with "====" and no error, or empty result with no errror.

1. Install SSL for service:

	`helm repo add nginx-stable https://helm.nginx.com/stable`
	
	`helm repo update`
	
	`helm install my-release nginx-stable/nginx-ingress`

1. Wait until apps are running by checking:

	`kubectl get pods`
	
	**successful result: ready shows 1/1 for all services**
	
1. Apply settings:

	`kubectl apply -f ingress.yaml`

	**Successful result: no errors**

1. Verify in browser by going to:

	[https://signingservice.local](https://signingservice.local)

	username: `admin`

	password: `change_password`

	**Successful result: able to login with default credentials**


