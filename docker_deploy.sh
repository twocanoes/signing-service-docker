#!/bin/bash

set -e

export TAG="latest"


while getopts "yt" flag
do
	case "${flag}" in
		y) skip_prompts=1
		;;
		
		t) export TAG="beta"
		;;
		
	esac
done

if [ "${skip_prompts}" ];  then 
	pull_recent="y"
else 
	printf "pull most recent docker image? [y/N]:"
	read pull_recent
fi
			
if [ "${pull_recent}" == "y" ]; then
	docker pull twocanoessoftware/signing-service:${TAG}
fi 

if [ "${skip_prompts}" ];  then 
	start_stop="y"
else 
	printf "stop and start docker? [Y/n]:"
	read start_stop
fi


if [ "${start_stop}" == "y" ]; then
	docker compose down
	docker compose up -d
fi 

if [ "${skip_prompts}" ];  then 
	run_rake="y"
else 

	printf "run rake tasks? [y/N]:"
	read run_rake
fi

if [ "${run_rake}" == "y" ]; then
	echo "sleep for 15 seconds to give db time to come up"
	sleep 15
	docker compose exec app rake db:migrate
	docker compose exec app rake assets:precompile
fi

